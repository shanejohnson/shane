from sense_hat import SenseHat
sense = SenseHat()
B = [0, 0, 0] # BLACK
HP = [153, 50, 204] # HOT PINK
DP = [255, 20, 147] # DEEP PINK
W = [255, 255, 255] # WHITE
BR = [165, 42, 42] # BROWN
sense.set_rotation(180) # flips the image 180 degrees
Heart = [
   B, B, B, B, B, B, W, B,
   B, B, B, B, B, B, BR, W,
   B, B, DP, B, B, DP, B, B,
   B, DP, HP, DP, DP, HP, DP, B,
   DP, HP, HP, HP, HP, HP, HP, DP,
   B, DP, HP, HP, HP, HP, DP, B,
   W, BR, DP, HP, HP, DP, B, B,
   W, W, B, DP, DP, B, B, B
]

sense.set_pixels(Heart)