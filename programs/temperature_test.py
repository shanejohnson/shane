from sense_hat import SenseHat
import os

# Create SenseHat object
sense = SenseHat()

# Get temp from SenseHat (it's in Celsius by default)
tempInCelsius = sense.get_temperature()
print("Temperature in Celsius:")
print(tempInCelsius)

# Get temperature of CPU since it affects sensor
t = os.popen('/opt/vc/bin/vcgencmd measure_temp')
cpuTemp = t.read()
cpuTemp = cpuTemp.replace('temp=','')
cpuTemp = cpuTemp.replace('\'C\n','')
cpuTemp = float(cpuTemp)
print("CPU Temp in Celsius:")
print(cpuTemp)

# Calculate better temperature using math
newTemp = tempInCelsius - ((cpuTemp - tempInCelsius) / 2) - 4
print("Better temperature in Celsius:")
print(newTemp)

# Use math to convert Celsius to Fahrenheit
tempInFahrenheit = newTemp * (9.0/5.0) + 32
print("Temperature in Fahrenheit:")
print(tempInFahrenheit)

# Round temperature to 2 decimal places
roundedTemp = round(tempInFahrenheit,2)
print("Rounded Temperature in Fahrenheit:")
print(roundedTemp)

# Convert rounded temperature into a string to display on SenseHat screen (string just means text instead of a number)
tempString = str(roundedTemp)
print("Temperature String:")
print(tempString)

# Rotate screen 180 degrees (only needed if it's upside down)
sense.set_rotation(180)

# Print message on SenseHat screen
sense.show_message("It's currently " + tempString + " degrees")