from sense_hat import SenseHat
sense = SenseHat()
Br = [139, 69, 19] # Brown  
B = [0, 191, 255] # Blue 
G = [0, 255, 0] # Green
Dg = [0, 100, 0] # Dark green
Y = [255, 255, 0] # Yellow
sense.set_rotation(180) # flips the image 180 degrees
Image = [
    B, B, B, B, B, B, Y, Y, 
    B, B, B, B, B, B, Y, Y,
    B, B, B, Dg, Dg, B, B, B,
    B, B, Dg, Dg, Dg, Dg, B, B,
    B, Dg, Dg, Dg, Dg, Dg, Dg, B,
    B, B, B, Br, Br, B, B, B, 
    B, B, B, Br, Br, B, B, B,
    G, G, G, G, G, G, G, G,
]

sense.set_pixels(Image)